## Message Protocol

An extension of the ideas in freechat - the intention is to make a base UDP protocol that implements the same P2P2P and encryption but allows expansion as necessary for whatever it is you're making. Making a game? Make a new packet type that handles gamedata, boom, done. etc.

Whereas freechat is a complete project and it's just waiting for further functionality, this is not that - this is very much WIP and will likely not work in it's current state.
