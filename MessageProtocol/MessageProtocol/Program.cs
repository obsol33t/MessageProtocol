﻿using System;


namespace MessageProtocol
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectionManager cm = new ConnectionManager(5757, 2757);
            Chat pC = new Chat(cm.makeNewPacket(Chat.SIGNATURE, cm.self));
            pC.message = "Hello world!";
            pC.updateBody();
            cm.pokeStun();
            while (!cm.receiverLoop.IsAlive)
            {
                System.Threading.Thread.Sleep(10);
            }
            cm.dialNeighbor(cm.self.getInPoint());
            System.Threading.Thread.Sleep(10);
            cm.dialNeighbor(cm.self.getOutPoint());
            System.Threading.Thread.Sleep(10);
            Console.ReadKey();
        }
    }
}
