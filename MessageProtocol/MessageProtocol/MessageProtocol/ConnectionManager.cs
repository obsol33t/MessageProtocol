﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LumiSoft.Net.STUN.Client;
using System.Net.Sockets;
using System.Net;
using System.Security.Cryptography;

namespace MessageProtocol
{
    /// <summary>
    /// 
    /// </summary>
    class ConnectionManager
    {
        #region Variables
        /// <summary>
        /// 
        /// </summary>
        public EndpointPair self;
        public string stunServer = "iphone-stun.strato-iphone.de";
        public int stunPort = 3478;
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<EndpointPair, BaseEndpoint> neighbors = new Dictionary<EndpointPair, BaseEndpoint>();
        /// <summary>
        /// 
        /// </summary>
        public UdpClient rxClient;
        /// <summary>
        /// 
        /// </summary>
        public UdpClient txClient;
        /// <summary>
        /// 
        /// </summary>
        public bool receiveLoop = false;
        /// <summary>
        /// 
        /// </summary>
        public bool transmitLoop = false;
        /// <summary>
        /// 
        /// </summary>
        public MIMOArray ReceivedQueue = new MIMOArray(false);
        /// <summary>
        /// 
        /// </summary>
        public bool encryption = false;
        /// <summary>
        /// 
        /// </summary>
        public Type encryptionProtocol = typeof(Rijndael);
        /// <summary>
        /// 
        /// </summary>
        public byte[] Key;
        /// <summary>
        /// 
        /// </summary>
        public byte[] IV;
        /// <summary>
        /// 
        /// </summary>
        public Thread receiverLoop;
        /// <summary>
        /// 
        /// </summary>
        public Thread senderLoop;
        #endregion


        #region PacketClassifier

        /// <summary>
        /// 
        /// </summary>
        public delegate GenericPacket PacketClassifier(GenericPacket p);

        /// <summary>
        /// 
        /// </summary>
        public PacketClassifier packetClassifier = GenericPacketClassifier;

        /// <summary>
        /// 
        /// </summary>
        public static GenericPacket GenericPacketClassifier(GenericPacket p)
        {
            if (p != null)
            {
                switch (p.header.getMode())
                {
                    /*
                           16: KeepAlive
                           17: NeighborBroadcast
                           18: Identity
                           19: Chat
                           20: MediaCtl
                     */

                    case 16: { return new KeepAlive(p); };
                    case 17: { return new NeighborBroadcast(p);  };
                    case 18: { return new Identity(p);  };
                    case 19: { return new Chat(p);  };
                    case 20: { return new MediaControl(p);  };
                    default: { return null;  }
                }
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public GenericPacket classifyPacket(GenericPacket p)
        {
            return packetClassifier.Invoke(p);
        }
        #endregion


        #region Constructors
        
        /// <summary>
        /// 
        /// </summary>
        public ConnectionManager(int inP, int outP)
        {
            rxClient = new UdpClient(new IPEndPoint(IPAddress.Any, inP));
            txClient = new UdpClient(new IPEndPoint(IPAddress.Any, outP));
            pokeStun();
            receiveLoop = true;
            receiverLoop = new Thread(new ThreadStart(listenLoop));
            receiverLoop.Start();
            transmitLoop = true;
            senderLoop = new Thread(new ThreadStart(sendLoop));
            senderLoop.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public GenericPacket makeNewPacket(bool isSend, int mode, EndpointPair sender, EndpointPair recipient, Type useType)
        {

            GenericPacket x = (GenericPacket)Activator.CreateInstance(useType, new object[] { new GenericPacket() });
            if (isSend)
            {
                x.header.setSendMsg();
            }
            else
            {
                x.header.setRecvMsg();
            }
            x.header.setMode((byte)mode);
            x.header.timeCode = new TimeCode(DateTime.Now);
            x.header.Sender = sender;
            x.header.Receiver = recipient;
            return x;
        }
        /// <summary>
        /// 
        /// </summary>
        public GenericPacket makeNewPacket(int mode, EndpointPair recipient, Type useType)
        {

            GenericPacket x = (GenericPacket)Activator.CreateInstance(useType, new object[] { new GenericPacket() });
            x.header.setSendMsg();
            x.header.setMode((byte)mode);
            x.header.timeCode = new TimeCode(DateTime.Now);
            x.header.Sender = self;
            x.header.Receiver = recipient;
            return x;
        }

        /// <summary>
        /// 
        /// </summary>
        public GenericPacket makeNewPacket(int mode, EndpointPair recipient)
        {

            GenericPacket x = new GenericPacket();
            x.header.setSendMsg();
            x.header.setMode((byte)mode);
            x.header.timeCode = new TimeCode(DateTime.Now);
            x.header.Sender = self;
            x.header.Receiver = recipient;
            return x;
        }
        #endregion


        #region Connection Functions
        
        /// <summary>
        /// 
        /// </summary>
        IPEndPoint queryStun(UdpClient udp)
        {
            // Query STUN server
            STUN_Result result = STUN_Client.Query(stunServer, stunPort, udp.Client);
            if (result.NetType == STUN_NetType.UdpBlocked)
            {
                // UDP blocked or !!!! bad STUN server
                return null;
            }
            else
            {
                IPEndPoint publicEP = result.PublicEndPoint;
                // Do your stuff
                return publicEP;

            }
        }

        /// <summary>
        /// poke stun
        /// </summary>
        public void pokeStun()
        {
            bool oldState = receiveLoop;
            receiveLoop = false;
            IPEndPoint x = queryStun(rxClient);
            IPEndPoint y = queryStun(txClient);
            x = queryStun(rxClient);
            y = queryStun(txClient);
            if (x != null && y != null) { 
                self.setIP(x.Address);
                self.PortIn = x.Port;
                self.PortOut = y.Port;
            }
            receiveLoop = oldState;
        }

        /// <summary>
        /// 
        /// </summary>
        public void pokeSelf()
        {
            KeepAlive ka = new KeepAlive(new GenericPacket());
            ka.header.setMode(KeepAlive.SIGNATURE);
            ka.header.setSendMsg();
            ka.header.timeCode = new TimeCode(DateTime.Now);
            ka.header.Sender = self;
            ka.header.Receiver = self;
            sendBytes(ka, self.getOutPoint(), rxClient);
            sendBytes(ka, self.getInPoint(), txClient);

        }

        /// <summary>
        /// 
        /// </summary>
        public void closeAll()
        { //Closes in and out clients
            txClient.Close();
            rxClient.Close();
        }
        #endregion


        #region Neighbor Functions
        // ping your neighbors

        /// <summary>
        /// 
        /// </summary>
        public void shareNeighbors()
        {
            NeighborBroadcast nb = new NeighborBroadcast();
            nb.header.Sender = self;
            foreach (BaseEndpoint n in neighbors.Values)
            {
                nb.addNeighborToPacket(n);
            }
            foreach (BaseEndpoint n in neighbors.Values)
            {
                NeighborBroadcast packet = new NeighborBroadcast((byte[])nb);
                packet.header.Receiver = new EndpointPair(n.InPoint, n.OutPoint);
                n.SendQueue.push(packet);

            }
        }
        //add a neighbor

        /// <summary>
        /// 
        /// </summary>
        public void instantiateNeighbor(EndpointPair s)
        {
            Identity i = new Identity();
            i.header.Sender = s;
            i.header.Receiver = self;
            BaseEndpoint be = new BaseEndpoint(s);
            be.who = i;
            be.lastSeen = DateTime.Now;
            neighbors.Add(s, be);

        }

        public void dialNeighbor(IPEndPoint neighbor)
        {
            Console.WriteLine("Reflecting from " + this.self.getOutPoint());
            STUN_Client.Reflect(stunServer, stunPort, this.txClient.Client, neighbor);
        }
        #endregion


        #region Send/Receive

        /// <summary>
        /// 
        /// </summary>
        public GenericPacket receiveBytes()
        {
            return receiveBytes(rxClient);
        }

        /// <summary>
        /// 
        /// </summary>
        public GenericPacket receiveBytes(UdpClient udp)
        {
            try
            {
                //IPEndPoint object will allow us to read datagrams sent from any source.
                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, self.PortIn);

                // Blocks until a message returns on this socket from a remote host.
                byte[] receiveBytes = udp.Receive(ref RemoteIpEndPoint);
                IPEndPoint pinger = null;
                //Try to parse as STUN
                try
                {
                    pinger = STUN_Client.ParseReflection(receiveBytes);
                }
                catch { }

                if (pinger != null)
                {
                    Console.WriteLine("Reflection received from " + pinger.Address.ToString() + ":" + pinger.Port.ToString());
                    return null;
                }
                //If not STUN
                else if (encryption)
                {
                    byte[] p = receiveBytes;
                    p= DecryptBytes(p, Key, IV, encryptionProtocol);
                    return p;
                }
                else
                {
                    GenericPacket p = new GenericPacket(receiveBytes);
                    return p;
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Console.WriteLine(e.ToString());
                Console.WriteLine();
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public bool sendBytes(byte[] message, EndpointPair recipient)
        {
            IPEndPoint recipientEP = recipient.getInPoint();
            return sendBytes(message, recipientEP, txClient);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool sendBytes(byte[] message, IPEndPoint ep, UdpClient udp)
        {
            try
            {
                if (encryption)
                {
                    byte[] sendbucket = EncryptBytes(message, Key, IV, encryptionProtocol);
                    udp.Send(sendbucket, sendbucket.Length, ep);
                }
                else
                {
                    // Sends a message to the host to which you have connected.
                    udp.Send(message, message.Length, ep);
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("");
                Console.WriteLine(e.ToString());
                Console.WriteLine("");
            }
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        public bool broadcastMessage(byte[] message)
        {
            try
            {
                foreach (BaseEndpoint b in neighbors.Values)
                {
                    b.SendQueue.push(message);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public bool queueMessage(byte[] message, BaseEndpoint b)
        {
            try
            {
                b.SendQueue.push(message);
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        #endregion


        #region ListenerThread

        /// <summary>
        /// 
        /// </summary>
        private void listenLoop()
        {
            while (true)
            {
                if (receiveLoop) { 
                    try
                    {
                        GenericPacket p = receiveBytes();
                        GenericPacket parsedP = packetClassifier.Invoke(p);
                        if (neighbors != null && parsedP!=null)
                        {
                            if (neighbors.ContainsKey(parsedP.header.Sender) || parsedP.header.Sender.IP == self.IP)
                            {
                                if (parsedP.header.getMode() != KeepAlive.SIGNATURE)
                                {
                                    lock (ReceivedQueue.array)
                                    {
                                        ReceivedQueue.push(parsedP);
                                    }
                                }
                            }
                        }
                        else
                        {

                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    finally
                    {
                        processReceipts();
                    }
                }
                else
                {
                    Thread.Sleep(10);
                }

            }
        }
        #endregion


        #region SenderThread

        /// <summary>
        /// 
        /// </summary>
        public void sendLoop()
        {
            while (true)
            {
                if (transmitLoop)
                {
                    lock (neighbors)
                    {
                        foreach (BaseEndpoint b in neighbors.Values)
                        {
                            int ceiling = b.SendQueue.array.Length;
                            for (int i = 0; i < ceiling; i++)
                            {
                                object msg = b.SendQueue.unshift();
                                GenericPacket message;
                                try
                                {
                                    message= new GenericPacket((byte[])msg);
                                }
                                catch(Exception e)
                                {
                                    message = (GenericPacket)msg;
                                }
                                sendBytes(message, b);
                                if (message.header.isSendMsg())
                                {
                                    b.SendQueue.shift(message);
                                }
                            }
                        }
                    }

                }
                Thread.Sleep(100);
            }

        }
        #endregion

        #region Queue Management Functions

        /// <summary>
        /// 
        /// </summary>
        public void processReceipts()
        {
            lock (ReceivedQueue.array)
            {
                try
                {
                    foreach (object oA in ((object[])ReceivedQueue.array))
                    {
                        if (((GenericPacket)oA).header.isSendMsg())
                        {
                            lock (neighbors)
                            {
                                if (neighbors != null)
                                {
                                    GenericPacket gP = new GenericPacket(oA);
                                    BaseEndpoint nB = neighbors[gP.header.Sender];
                                    gP.header.setRecvMsg();
                                    gP.body = new byte[0];
                                    nB.SendQueue.push(gP);
                                }
                            }
                        }
                        else
                        {
                            GenericPacket gP = new GenericPacket(oA);
                            gP.header.setSendMsg();
                            lock (neighbors)
                            {
                                if (neighbors != null)
                                {
                                    BaseEndpoint nB = neighbors[gP.header.Receiver];
                                    int offset = -1;
                                    for (int i = 0; i < nB.SendQueue.array.Count(); i++)
                                    {
                                        byte[] currentBytePack =(byte[])nB.SendQueue.array[i];
                                        GenericPacket currentPacket = currentBytePack;

                                        if (currentPacket.header.ModeAndTime == gP.header.ModeAndTime &&
                                            currentPacket.header.Sender.IP == gP.header.Sender.IP &&
                                            currentPacket.header.Receiver.IP == gP.header.Receiver.IP)
                                        {
                                            offset = i;
                                        }
                                    }
                                    if (offset > 0)
                                    {
                                        object[] nBucket = new object[nB.SendQueue.array.Length - 1];
                                        Array.Copy(nB.SendQueue.array, 0, nBucket, 0, offset - 1);
                                        Array.Copy(nB.SendQueue.array, offset, nBucket, 0, nB.SendQueue.array.Length - offset-1);
                                        nB.SendQueue.array = nBucket;
                                    }
                                }
                            }

                        }
                    }
                }
                catch (Exception e) { }
            }
        }
        #endregion

        //public static IPEndPoint stringToEndpoint(String ep) //Something to encode/decode endpoint fortransition


        #region Encryption Function

        /// <summary>
        /// 
        /// </summary>
        public static string generateKey()
        {
            Rijndael rij = Rijndael.Create();
            return (Convert.ToBase64String(rij.Key) + ":" + Convert.ToBase64String(rij.IV));
        }

        /// <summary>
        /// 
        /// </summary>
        public static byte[] EncryptBytes(byte[] bArray, byte[] Key, byte[] IV, Type algoType)
        {
            // Check arguments.
            if (bArray == null || bArray.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an Rijndael object
            // with the specified key and IV.
            using (SymmetricAlgorithm rijAlg = (SymmetricAlgorithm)algoType.GetMethod("Create").Invoke(null,null))
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (BinaryWriter swEncrypt = new BinaryWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(bArray);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }


        /// <summary>
        /// 
        /// </summary>
        public static byte[] DecryptBytes(byte[] cipherText, byte[] Key, byte[] IV, Type algoType)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            MIMOArray bucket = new MIMOArray(false);
            try
            {
                // Create an Rijndael object
                // with the specified key and IV.
                using (SymmetricAlgorithm rijAlg = (SymmetricAlgorithm)algoType.GetMethod("Create").Invoke(null, null))
                {
                    rijAlg.Key = Key;
                    rijAlg.IV = IV;

                    // Create a decryptor to perform the stream transform.
                    ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                    // Create the streams used for decryption.
                    using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (BinaryReader srDecrypt = new BinaryReader(csDecrypt))
                            {

                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                try
                                {
                                    while (true)
                                    {
                                        bucket.push(srDecrypt.ReadByte());
                                    }
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            byte[] result = new byte[bucket.array.Length];
            for (int i = 0; i < bucket.array.Length; i++)
            {
                result[i] = (byte)bucket.array[i];
            }
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        public static byte[] EncryptBytes(byte[] bArray, string keyString, Type algoType)
        {
            // Check arguments.
            if (bArray == null || bArray.Length <= 0)
                throw new ArgumentNullException("plainText");
            byte[] Key = Convert.FromBase64String(keyString.Split(':')[0]);
            byte[] IV = Convert.FromBase64String(keyString.Split(':')[1]);
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            return EncryptBytes(bArray, Key, IV, algoType);
        }


        /// <summary>
        /// 
        /// </summary>
        public static byte[] DecryptBytes(byte[] cipherText, string keyString, Type algoType)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            byte[] Key = Convert.FromBase64String(keyString.Split(':')[0]);
            byte[] IV = Convert.FromBase64String(keyString.Split(':')[1]);
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            return DecryptBytes(cipherText, Key, IV, algoType);
        }
        #endregion
    }


}
