﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageProtocol
{
    class KeepAlive : GenericPacket
    {

        public long receivedTime;
        public new const int SIGNATURE = 16;
        public KeepAlive(byte[] b) : base(b)
        {
            receivedTime = DateTime.Now.Ticks;
        }
    }
}
