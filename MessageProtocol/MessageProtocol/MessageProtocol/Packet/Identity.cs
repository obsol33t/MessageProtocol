﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace MessageProtocol
{
    class Identity : GenericPacket
    {
        public new const int SIGNATURE = 18;
        public System.Drawing.Color color = System.Drawing.Color.FromArgb(255,128,128,128);
        public string name = "Anonymous";

        public Identity() : base()
        {
            this.header.setMode(SIGNATURE);
            updateBody();
        }
        public Identity(byte[] b) : base(b)
        {
            if(((GenericPacket)b).header.Mode == SIGNATURE)
            {
                updateValues();
            }

        }
        public void updateBody()
        {
            this.setBody((color.ToArgb() + ":" + WebUtility.UrlEncode(name)));
        }
        public void updateValues()
        {
            if (this.body.Length > 0)
            {
                string x = this.getBodyString();
                int c = int.Parse(x.Split(':')[0]);
                color = System.Drawing.Color.FromArgb(c);
                name = WebUtility.UrlDecode(x.Split(':')[1]);
            }
        }
        public override string ToString()
        {
            return WebUtility.HtmlEncode(name);
        }
    }
}
