﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageProtocol
{
    class MediaControl : GenericPacket
    {
        public new const int SIGNATURE = 20;
        public string message = "";

        public MediaControl(byte[] b) : base(b)
        {
            if (this.body.Length > 0)
            {
                string x = this.getBodyString();

                message = System.Net.WebUtility.UrlDecode(x);
            }
            updateBody();
        }
        public override string ToString()
        {
            return System.Net.WebUtility.HtmlEncode(message);
            
        }
        public void updateBody()
        {
            this.setBody(System.Net.WebUtility.UrlEncode(message));
        }
    }
}
