﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageProtocol
{
    class NeighborBroadcast : GenericPacket
    {
        public new const int SIGNATURE = 17;
        public string message = "";

        public NeighborBroadcast(byte[] b) : base(b)
        {
            if (this.body.Length > 0)
            {
                string x = this.getBodyString();

                message = System.Net.WebUtility.UrlDecode(x);
            }
            updateBody();
        }
        public NeighborBroadcast() : base(new GenericPacket())
        {

        }
        public override string ToString()
        {
            return System.Net.WebUtility.HtmlEncode(message);
        }
        public void updateBody()
        {
            this.setBody(System.Net.WebUtility.UrlEncode(message));
        }
        public MIMOArray getNeighbors()
        {
            MIMOArray neighbors = new MIMOArray();
            string[] neighborStrs = message.Split(";");
            foreach(string n in neighborStrs)
            {
                BaseEndpoint be = new BaseEndpoint(System.Convert.FromBase64String(n));
                neighbors.push(be);
            }
            return neighbors;
        }
        public void addNeighborToPacket(BaseEndpoint be)
        {
            message += System.Convert.ToBase64String(be.EncodeAddress());
        }
    }
}
