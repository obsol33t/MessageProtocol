﻿using System;
using System.Collections.Generic;
using System.Text;

/*
    Message Deinition:
   
    MesssageType
        A:IO Flag    Get=&128, Set=|128
	    B:Mode Flag  Get=&127, Set=x&127
    TimeCode/MagicString
        M:Minutes
        S:Seconds
        m:Milli
    Sender Endpoint
        C:IP
        D:InPort
        E:OutPort
    Receiver Endpoint
        C:IP
        D:InPort
        E:OutPort

    ABBBBBBB MMMMMMMM SSSSSSSS mmmmmmmm
    CCCCCCCC.CCCCCCCC.CCCCCCCC.CCCCCCCC
    DDDDDDDD DDDDDDDD DDDDDDDD DDDDDDDD
    EEEEEEEE EEEEEEEE EEEEEEEE EEEEEEEE
    FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF
    GGGGGGGG GGGGGGGG GGGGGGGG GGGGGGGG
    HHHHHHHH HHHHHHHH HHHHHHHH HHHHHHHH

    Mode Range:
    0-127: Full Range
     0-14: Reserved for future use
       16: KeepAlive
       17: NeighborBroadcast
       18: Identity
       19: Chat
       20: MediaCtl
      >20: Unused
*/

namespace MessageProtocol
{
    /// <summary>
    /// 
    /// </summary>
    class GenericPacket
    {
        /// <summary>
        /// 
        /// </summary>
        public PacketHeader header;

        /// <summary>
        /// 
        /// </summary>
        public byte[] body;

        /// <summary>
        /// 
        /// </summary>
        public const int SIGNATURE = 0;

        #region Constructors

        /// <summary>
        /// Creates a generic packet to nowhere
        /// </summary>
        public GenericPacket()
        {
            header = new PacketHeader((PacketHeader.IOFlag & GenericPacket.SIGNATURE),
                new System.Net.IPEndPoint(System.Net.IPAddress.Parse("127.0.0.1"), 5001),
                new System.Net.IPEndPoint(System.Net.IPAddress.Parse("127.0.0.1"), 5002),
                new System.Net.IPEndPoint(System.Net.IPAddress.Parse("127.0.0.1"), 5003),
                new System.Net.IPEndPoint(System.Net.IPAddress.Parse("127.0.0.1"), 5004));
            body =new byte[0];
        }

        /// <summary>
        /// 
        /// </summary>
        public GenericPacket(object o)
        {
            try
            {
                GenericPacket temp = (GenericPacket)o;
                header = temp.header;
                body = temp.body;
            }
            catch(Exception e){
                GenericPacket temp = new GenericPacket((byte[])o);
                header = temp.header;
                body = temp.body;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public GenericPacket(byte[] b)
        {
            if (b.Length > 27)
            {
                byte[] headB = new byte[28];
                byte[] bodyB = new byte[b.Length - 28];
                Array.Copy(b, 0, headB, 0, 28);
                header = headB;

                Array.Copy(b, 28, bodyB, 0, bodyB.Length);
                body = bodyB;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public GenericPacket(PacketHeader b)
        {
            header = b;
            body = new byte[1];
            body[0] = (byte)(0);
        }
        #endregion

        #region Part functions

        /// <summary>
        /// 
        /// </summary>
        public void setBody(byte[] b)
        {
            body = b;
        }

        /// <summary>
        /// 
        /// </summary>
        public void setBody(string message)
        {
            body = ASCIIEncoding.ASCII.GetBytes(message);
        }

        /// <summary>
        /// 
        /// </summary>
        public string getBodyString()
        {
            return Encoding.ASCII.GetString(body);
        }

        /// <summary>
        /// 
        /// </summary>
        public static PacketHeader GetHeader(byte[] b)
        {
            byte[] headB = new byte[28];
            Array.Copy(b, 0, headB, 0, 28);
            return headB;
        }
        #endregion

        #region Casters

        /// <summary>
        /// 
        /// </summary>
        public static implicit operator byte[](GenericPacket value)
        {
            if (value.header.isSendMsg()) { 
                byte[] packet = new byte[((byte[])value.header).Length + value.body.Length];
                Array.Copy(value.header, 0, packet, 0, ((byte[])value.header).Length);
                Array.Copy(value.body, 0, packet, ((byte[])value.header).Length, value.body.Length);
                return packet;
            }
            else
            {
                return value.header;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public static implicit operator GenericPacket(byte[] value)
        {
            return new GenericPacket(value);

        }

        /// <summary>
        /// 
        /// </summary>
        public override string ToString()
        {
            return Convert.ToBase64String(this.header)+":"+Encoding.ASCII.GetString(body);
        }
        #endregion
    }
}
