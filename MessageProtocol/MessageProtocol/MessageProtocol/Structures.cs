﻿using System;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Explicit)]
struct Int32Converter
{
    [FieldOffset(0)] public Int32 Value;
    [FieldOffset(0)] public byte Byte1;
    [FieldOffset(1)] public byte Byte2;
    [FieldOffset(2)] public byte Byte3;
    [FieldOffset(3)] public byte Byte4;

    public Int32Converter(Int32 value)
    {
        Byte1 = Byte2 = Byte3 = Byte4 = 0;
        Value = value;
    }
    public Int32Converter(byte[] value)
    {
        Value = 0;
        Byte1 = value[0];
        Byte2 = value[1];
        Byte3 = value[2];
        Byte4 = value[3];

    }

    public static implicit operator Int32(Int32Converter value)
    {
        return value.Value;
    }
    public static implicit operator byte[](Int32Converter value)
    {
        return new byte[]{
            value.Byte1,
            value.Byte2,
            value.Byte3,
            value.Byte4
            };
    }

    public static implicit operator Int32Converter(Int32 value)
    {
        return new Int32Converter(value);
    }
}


[StructLayout(LayoutKind.Explicit)]
struct TimeCode {
    [FieldOffset(0)] public byte MinuteByte;
    [FieldOffset(1)] public byte SecondByte;
    [FieldOffset(2)] public byte MilliByte;
    /// <summary>
    /// Structs needs you to assign EVERYTHING to assign anything. So this is a cheap hack so there's a constructor to set the blank struct before populating it. Bool does nothing.
    /// </summary>
    public TimeCode(bool CheapHack)
    {
        MinuteByte = SecondByte = MilliByte = 0;
    }
    public TimeCode(DateTime dt)
    {
        this = new TimeCode(false);
        Int32Converter mil = new Int32Converter(dt.Millisecond);
        MilliByte = new Int32Converter(dt.Millisecond).Byte1;
        SecondByte = new Int32Converter(dt.Second).Byte1;
        MinuteByte = new Int32Converter(dt.Minute).Byte1;
    }
    public static implicit operator byte[](TimeCode value)
    {
        return new byte[] { value.MinuteByte, value.SecondByte, value.MilliByte };
    }
    public static implicit operator TimeCode(byte[] value)
    {
        TimeCode x = new TimeCode(false);
        x.MinuteByte = value[0];
        x.SecondByte = value[1];
        x.MilliByte = value[2];
        return x;
    }
}

[StructLayout(LayoutKind.Explicit)]
struct EndpointPair
{
    [FieldOffset(0)] public Int32 IP;
    [FieldOffset(0)] public byte IP1;
    [FieldOffset(1)] public byte IP2;
    [FieldOffset(2)] public byte IP3;
    [FieldOffset(3)] public byte IP4;
    [FieldOffset(4)] public Int32 PortIn;
    [FieldOffset(4)] public byte PortIn1;
    [FieldOffset(5)] public byte PortIn2;
    [FieldOffset(6)] public byte PortIn3;
    [FieldOffset(7)] public byte PortIn4;
    [FieldOffset(8)] public Int32 PortOut;
    [FieldOffset(8)] public byte PortOut1;
    [FieldOffset(9)] public byte PortOut2;
    [FieldOffset(10)] public byte PortOut3;
    [FieldOffset(11)] public byte PortOut4;


    //Set what matters
    /// <summary>
    /// Structs needs you to assign EVERYTHING to assign anything. So this is a cheap hack so there's a constructor to set the blank struct before populating it. Bool does nothing.
    /// </summary>
    public EndpointPair(bool cheapHack)
    {
        IP=0;
        IP1=0;
        IP2=0;
        IP3=0;
        IP4=0;
        PortIn=0;
        PortIn1=0;
        PortIn2=0;
        PortIn3=0;
        PortIn4=0;
        PortOut=0;
        PortOut1=0;
        PortOut2=0;
        PortOut3=0;
        PortOut4=0;
    }
    public EndpointPair(System.Net.IPAddress ip, int portIn, int portOut)
    {
        // I am done with this having to set everything before setting what matters. 
        this = new EndpointPair(true);
        byte[] ipB = ip.GetAddressBytes();
        IP1 = ipB[0]; IP2 = ipB[1]; IP3 = ipB[2]; IP4 = ipB[3];
        PortIn = portIn;
        PortOut = portOut;
    }
    public EndpointPair(System.Net.IPEndPoint inp, System.Net.IPEndPoint outp)
    {
        this = new EndpointPair(inp.Address, inp.Port, outp.Port);
    }
    public EndpointPair(byte[] b)
    {
        // I am done with this having to set everything before setting what matters. 
        this = new EndpointPair(true);
        int offset = 0;
        IP1 = b[offset++]; IP2 = b[offset++]; IP3 = b[offset++]; IP4 = b[offset++];
        PortIn = new Int32Converter((new byte[] { b[offset++], b[offset++], b[offset++], b[offset++] }));
        PortOut = new Int32Converter((new byte[] { b[offset++], b[offset++], b[offset++], b[offset++] }));

    }
    public System.Net.IPEndPoint getInPoint()
    {
        byte[] ipB = new byte[] { IP1, IP2, IP3, IP4 };
        return new System.Net.IPEndPoint(new System.Net.IPAddress(ipB), PortIn);
    }
    public System.Net.IPEndPoint getOutPoint()
    {
        byte[] ipB = new byte[] { IP1, IP2, IP3, IP4 };
        return new System.Net.IPEndPoint(new System.Net.IPAddress(ipB), PortOut);
    }

    public static implicit operator byte[](EndpointPair value)
    {
        return new byte[]
        {
            value.IP1,
            value.IP2,
            value.IP3,
            value.IP4,
            value.PortIn1,
            value.PortIn2,
            value.PortIn3,
            value.PortIn4,
            value.PortOut1,
            value.PortOut2,
            value.PortOut3,
            value.PortOut4
        };
    }

    /// <summary>
    /// lets byte arrays be quick-cast to packetheaders if that's what the function expects - handy quick serialization for recieving
    /// </summary>
    /// <param name="b"></param>
    public static implicit operator EndpointPair(byte[] b)
    {
        return new EndpointPair(b);
    }

    /// <summary>
    /// Set the IP
    /// </summary>
    public void setIP(System.Net.IPAddress ip)
    {
        IP = IPtoInt(ip);
    }
    /// <summary>
    /// utility function to turn an IP to an INT
    /// </summary>
    public static int IPtoInt(System.Net.IPAddress ip)
    {

        byte[] address = ip.GetAddressBytes();
        Int32Converter ic = 0;
        ic.Byte1 = address[0];
        ic.Byte2 = address[1];
        ic.Byte3 = address[2];
        ic.Byte4 = address[3];
        return ic.Value;
    }
}
    /// <summary>
    /// This struct represents the header part of a packet. Contains IO, mode, timecode, and sender and receiver info
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
struct PacketHeader
{
    /// <summary>
    /// When set, sending. Otherwise, receiving. (x & ioflag)==ioflag
    /// </summary>
    public const byte IOFlag = 128; //When set, sending. Otherwise, receiving.
    /// <summary>
    /// seperates mode from io by &
    /// </summary>
    public const byte ModeFlag = 127;
    /// <summary>
    /// Combined Mode and Time int
    /// </summary>
    [FieldOffset(0)] public Int32 ModeAndTime;
    /// <summary>
    /// Mode byte (type of packet + io direction)
    /// </summary>
    [FieldOffset(0)] public byte Mode;
    /// <summary>
    /// Time code
    /// </summary>
    [FieldOffset(1)] public TimeCode timeCode;
    /// <summary>
    /// Sender
    /// </summary>
    [FieldOffset(4)] public EndpointPair Sender;
    /// <summary>
    /// Receiver
    /// </summary>
    [FieldOffset(16)] public EndpointPair Receiver;

    /// <summary>
    /// Provide combined mode and time as int, ip as int instead of byte[], port as int. Shouldn't be useful ever.
    /// </summary>
    /// <param name="modeandtime"></param>
    /// <param name="senderip"></param>
    /// <param name="senderinport"></param>
    /// <param name="senderoutport"></param>
    /// <param name="receiverip"></param>
    /// <param name="receiverinport"></param>
    /// <param name="receiveroutport"></param>
    public PacketHeader(int modeandtime,
        System.Net.IPAddress senderip, int senderinport, int senderoutport,
        System.Net.IPAddress receiverip, int receiverinport, int receiveroutport)
    {
        Mode = 0;
        timeCode = new TimeCode(false);
        ModeAndTime = modeandtime;
        Sender = new EndpointPair(senderip, senderinport, senderoutport);
        Receiver = new EndpointPair(receiverip, receiverinport, receiveroutport);
    }

    /// <summary>
    /// Give this the mode and timecode, and the pair in and out endpoints
    /// </summary>
    /// <param name="modeandtime"></param>
    /// <param name="SenderIn"></param>
    /// <param name="SenderOut"></param>
    /// <param name="ReceiverIn"></param>
    /// <param name="ReceiverOut"></param>
    public PacketHeader(int modeandtime, 
        System.Net.IPEndPoint SenderIn,
        System.Net.IPEndPoint SenderOut,
        System.Net.IPEndPoint ReceiverIn,
        System.Net.IPEndPoint ReceiverOut)
    {
        Mode = 0;
        timeCode = new TimeCode(false);
        ModeAndTime = modeandtime;
        Sender = new EndpointPair(SenderIn.Address, SenderIn.Port, SenderOut.Port);
        Receiver = new EndpointPair(ReceiverIn.Address, ReceiverIn.Port, ReceiverOut.Port);

    }

    /// <summary>
    /// This'll take a raw byte array, and shouldn't have to care about length - it just takes the head and ignores the rest
    /// </summary>
    /// <param name="b"></param>
    public PacketHeader(byte[] b)
    {
        ModeAndTime = 0;
        int offset = 0;
        Mode = b[offset++];
        timeCode.MinuteByte = b[offset++];
        timeCode.SecondByte = b[offset++];
        timeCode.MilliByte = b[offset++];

        Sender = new EndpointPair((new byte[] {
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++]
        }));
        Receiver = new EndpointPair((new byte[] {
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++],
         b[offset++]
        }));

    }
    /// <summary>
    /// Create a header from a modeandflag and 2 endpointpairs
    /// </summary>
    public PacketHeader(int modeandflag, EndpointPair s, EndpointPair r)
    {
        ModeAndTime = 0;
        Mode = ((byte)modeandflag);
        timeCode = new TimeCode();

        Sender = s;
        Receiver = r;

    }

    /// <summary>
    /// Checks if the Mode contains a set IO flag (Send)
    /// </summary>
    /// <returns></returns>
    public bool isSendMsg()
    {
        return ((Mode & IOFlag) ==IOFlag);
    }
    /// <summary>
    /// Checks if the Mode contains an unset IO flag (Recv)
    /// </summary>
    /// <returns></returns>
    public bool isRecvMsg()
    {
        return ((Mode & IOFlag) != IOFlag);
    }
    /// <summary>
    /// Sets the Mode IO flag to send
    /// </summary>
    public void setSendMsg()
    {
        Mode |= IOFlag;
    }
    /// <summary>
    /// Sets the Mode IO flag to recv
    /// </summary>
    public void setRecvMsg()
    {
        Mode &= ModeFlag;
    }
    /// <summary>
    /// Sets the rest of the mode (outside of the IO flag)
    /// </summary>
    /// <param name="mode"></param>
    public void setMode(byte mode)
    {
        Mode &= IOFlag;
        Mode |= mode;

    }
    /// <summary>
    /// returns the mode, sans-IO flag
    /// </summary>
    /// <returns></returns>
    public byte getMode()
    {
        byte mode = Mode;
        mode &= ModeFlag;
        return mode;
    }
    
    /// <summary>
    /// letspacketheaders be quick-cast to byte arrays if that's what the function expects - handy quick serialization for sending
    /// </summary>
    /// <param name="value"></param>
    public static implicit operator byte[](PacketHeader value)
    {
        return new byte[]
        {
            value.Mode,
            value.timeCode.MinuteByte,
            value.timeCode.SecondByte,
            value.timeCode.MilliByte,
            value.Sender.IP1,
            value.Sender.IP2,
            value.Sender.IP3,
            value.Sender.IP4,
            value.Sender.PortIn1,
            value.Sender.PortIn2,
            value.Sender.PortIn3,
            value.Sender.PortIn4,
            value.Sender.PortOut1,
            value.Sender.PortOut2,
            value.Sender.PortOut3,
            value.Sender.PortOut4,
            value.Receiver.IP1,
            value.Receiver.IP2,
            value.Receiver.IP3,
            value.Receiver.IP4,
            value.Receiver.PortIn1,
            value.Receiver.PortIn2,
            value.Receiver.PortIn3,
            value.Receiver.PortIn4,
            value.Receiver.PortOut1,
            value.Receiver.PortOut2,
            value.Receiver.PortOut3,
            value.Receiver.PortOut4,
        };
    }

    /// <summary>
    /// lets byte arrays be quick-cast to packetheaders if that's what the function expects - handy quick serialization for recieving
    /// </summary>
    /// <param name="b"></param>
    public static implicit operator PacketHeader(byte[] b)
    {
        return new PacketHeader(b);
    }
}



/// <summary>
/// I didn't feel like dealing with array shuffling and wanted to be able to insert at either end
/// </summary>
struct MIMOArray
{
    /// <summary>
    /// Internal array
    /// </summary>
    public object[] array;
    /// <summary>
    /// Create a MIMO array from an array
    /// </summary>
    public MIMOArray(object[] a)
    {
        array = a;
    }

    /// <summary>
    /// Create an empty MIMO array (bool doesn't matter)
    /// </summary> 
    public MIMOArray(bool Cheat)
    {
        array = new object[0];
    }

    /// <summary>
    /// Remove and return the first item
    /// </summary>
    public object pop()
    {
        object[] bucket = new object[array.Length-1];
        object returnObj = array[array.Length - 1];
        Array.Copy(array, 1, bucket, 0, bucket.Length);
        array = bucket;
        return returnObj;
    }

    /// <summary>
    /// Remove and return the last
    /// </summary>
    public object unshift()
    {
        object[] bucket = new object[array.Length-1];
        object returnObj = array[0];
        Array.Copy(array, 0, bucket, 0, bucket.Length);
        array = bucket;
        return returnObj;
    }

    /// <summary>
    /// add to the end (I think I messed this up)
    /// </summary>
    public void push(object o)
    {
        object[] bucket = new object[array.Length+1];
        Array.Copy(array, 0, bucket, 0, array.Length);
        bucket[array.Length] = o;
        array = bucket;
    }

    /// <summary>
    /// add to beginning
    /// </summary>
    public void shift(object o)
    {
        object[] bucket = new object[array.Length + 1];
        Array.Copy(array, 1, bucket, 0, array.Length);
        bucket[0] = o;
        array = bucket;
    }
}