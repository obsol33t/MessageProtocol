﻿using System;
using System.Net;
namespace MessageProtocol
{
    /// <summary>
    /// 
    /// </summary>
    class BaseEndpoint
    {

        /// <summary>
        /// 
        /// </summary>
        public IPEndPoint InPoint;
        /// <summary>
        /// 
        /// </summary>
        public IPEndPoint OutPoint;

        /// <summary>
        /// 
        /// </summary>
        public DateTime lastSeen = new DateTime();

        /// <summary>
        /// 
        /// </summary>
        public Identity who = new Identity(new GenericPacket());

        /// <summary>
        /// 
        /// </summary>
        public MIMOArray SendQueue = new MIMOArray(false);


        /// <summary>
        /// 
        /// </summary>
        public BaseEndpoint(byte[] addr)
        {
            DecodeAddress(addr);
        }

        /// <summary>
        /// 
        /// </summary>
        public BaseEndpoint(IPAddress ip, int iPo, int oPo)
        {
            InPoint = new IPEndPoint(ip, iPo);
            OutPoint = new IPEndPoint(ip, oPo);
        }

        /// <summary>
        /// 
        /// </summary>
        public BaseEndpoint(IPEndPoint inp, IPEndPoint outP)
        {
            InPoint = inp;
            OutPoint = outP;
        }

        /// <summary>
        /// 
        /// </summary>
        public BaseEndpoint(EndpointPair ep)
        {
            InPoint = ep.getInPoint();
            OutPoint = ep.getOutPoint();
        }

        /// <summary>
        /// 
        /// </summary>
        public bool DecodeAddress(byte[] addr)
        {
            try
            {
                byte[] ip = new byte[4];
                Array.Copy(addr, 0, ip, 0, ip.Length);
                Int32Converter port1 = new Int32Converter(0);
                Int32Converter port2 = new Int32Converter(0);
                int offset = ip.Length;
                port1.Byte1 = addr[offset++];
                port1.Byte2 = addr[offset++];
                port1.Byte3 = addr[offset++];
                port1.Byte4 = addr[offset++];
                port2.Byte1 = addr[offset++];
                port2.Byte2 = addr[offset++];
                port2.Byte3 = addr[offset++];
                port2.Byte4 = addr[offset++];
                InPoint = new IPEndPoint((new IPAddress(ip)), port1);
                OutPoint = new IPEndPoint((new IPAddress(ip)), port2);
                return true;
            }
            catch (Exception e){
                Console.WriteLine("Failed to decode address. Byte array:");
                for(int i = 0; i < addr.Length; i++)
                {
                    Console.WriteLine((int)addr[i]);
                };
                Console.WriteLine("vvv=====RESULT=====vvv");
                Console.WriteLine(e.ToString());
                Console.WriteLine("AAA=====RESULT=====AAA");
                Console.WriteLine("");
                Console.WriteLine("");
                return false;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public byte[] EncodeAddress()
        {
            byte[] ip = InPoint.Address.GetAddressBytes();
            Int32Converter port1 = new Int32Converter(InPoint.Port);
            Int32Converter port2 = new Int32Converter(OutPoint.Port);
            byte[] encodedAddress = new byte[(ip.Length + ((byte[])port1).Length + ((byte[])port2).Length)];
            Array.Copy(ip, 0, encodedAddress, 0, ip.Length);
            Array.Copy(port1, 0, encodedAddress, (ip.Length), ((byte[])port1).Length);
            Array.Copy(port2, 0, encodedAddress, (ip.Length + ((byte[])port1).Length), ((byte[])port2).Length);
            return encodedAddress;
        }

        /// <summary>
        /// 
        /// </summary>
        public static byte[] EncodeAddress(IPEndPoint inP, IPEndPoint outP)
        {

            byte[] ip = inP.Address.GetAddressBytes();
            Int32Converter port1 = new Int32Converter(inP.Port);
            Int32Converter port2 = new Int32Converter(outP.Port);
            byte[] encodedAddress = new byte[(ip.Length + ((byte[])port1).Length + ((byte[])port2).Length)];
            Array.Copy(ip, 0, encodedAddress, 0, ip.Length);
            Array.Copy(port1, 0, encodedAddress, (ip.Length), ((byte[])port1).Length);
            Array.Copy(port2, 0, encodedAddress, (ip.Length + ((byte[])port1).Length), ((byte[])port2).Length);
            return encodedAddress;
        }


        /// <summary>
        /// 
        /// </summary>
        public static implicit operator EndpointPair(BaseEndpoint be)
        {
            return new EndpointPair(be.InPoint.Address, be.InPoint.Port, be.OutPoint.Port);
        }

        
    }
}
